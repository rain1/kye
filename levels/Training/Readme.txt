
			     	  **********************
			       	  *Kye Training Package*
			       	  **********************


	       |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|
	       |To view this document make sure that edit/Word wrap is on|
               |							 |
	        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



       *----------------------*
       * 1.	Agreement     *
       * 2. 	Contact       *
       * 3.     Other Freeware*
       * 4. 	Thanks        *
       * 5.     About         *
       * 6.     Levels        *
       *----------------------*


*---*
*1. *
*---*

1. This is freeware so you can use it yourself or let others try it but I don't want you SELLING IT!!!
2. I will let you adjust the levels a bit but for personnel use ONLY!!!
3. If you like it you MUST SEND ME �100 OR $120. Only joking!!!!!! But please E-mail me if you like it or not so I can make adjustments (my e-mail address is in the contact section)
4. This pack has been tested for any errors or viruses therefore I will be in NO WAY responsible for any damage that may become of using this Package.


*---*
*2. *
*---*

My E-mail Address is:
tjyorkshire@yahoo.co.uk

Please E-mail me if you have a problem, suggest some improvements or just to tell me if you like it or not.


*---*
*3. *
*---*

I am going to make another Kye training package but I need help from you. If you can create a good training level e-mail it to me with you name and if it is as good as you say I will put it on my next training package. I will notify you if I think it is good enough with an e-mail and send you some samples of the training package.


*---*
*4. *
*---*

Special Thanks to:

*----------|---------------------------------------------------*
*My Sister |  For Making level 11 and testing all the levels   *
*My Nan    |  For Quality Testing                              *
*and me    |  For Making levels 1-10                           *
*----------|---------------------------------------------------*



*---*
*5. *
*---*

These 11 Levels are for those people who have just got the game Kye or who are just rubbish at it. When you open the Training.kye you will start with a welcome level where all you have to do is to get the 3 diamonds in the middle. You will then be put onto other training levels that will get harder as you go along. Once you have complete the levels you should now be a whiz at Kye and should whiz through the game.

Please read the level details below as it will help you.


*---*
*6. *
*---*

Level 1 (Welcome): This is just a practise level.

Level 2 (Fiddle): This is a level where you can get to know what the objects on Kye do. Once you have had a look and played with them you can get the diamond.

Level 3 (Laser): These Lasers looking doors only let you go through them one way. Try and find weak points in the to get the diamond.

Level 4 (Blackys): These black squares take a life of you if you go into them. Try and go around them to get to the diamond.

Level 5 (Sticky Blocks): These blocks stick to rockys (The round arrow blocks) to move them out of the way of a door a gap in the wall etc. Use these sticky blocks to get the rockys out of the way so you can get through the doors.

Level 6 (Reactions): On this level you have to move the earth (the faint yellow blocks) to let the rockys, sliders etc out. See what happens when the hit the clockers (the square blocks at the right wall).

Level 7 (Timers): Timers are blocks that dissolve after the time displayed on them. Try and get the diamond go into the shelter get the other diamond after the time runs out without getting trapped.

Level 8 (Beasts): Beasts are evil bug things that try to kill you. Try and navigate through the earth without setting and beasts free.

Level 9 (Eruption): On this level you have got get the diamonds without being trapped by the rockys. Tip: Get the top diamonds first.

Level 10 (Everything) on this level I have put a bit of everything you have covered on this training package.

Level 11 (Solid as a Rock) This is a very hard level if you can do it you are a Kye WIZZ!!!

For hints, tips and tricks E-mail me telling me what level your on and what you need help with.

31/05/03

(C) 2003
