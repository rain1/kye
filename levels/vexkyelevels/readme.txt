     Copyright © 2011 Victor Hugo Soliz Kuncar
     
     Copying and distribution of this Kye Level pack, with or without modification,
     are permitted in any medium without royalty provided the copyright
     notice and this notice are preserved.

---------------
 vex.kye

Some basic to intermediate levels. I am releasing these levels because it has gotten difficult to find easy and intermediate Kye levels that are freely distributable and officially have a license that makes us sure of that.

Here are the level names in case you'd like to skip a level and some descriptions:
* TIME: This is just a demo of pushers and clocks.
* SLIDE, DONTSLIDE : Simple requests of knowledge about round blocks.
* FOUR: Slightly interesting arrow puzzle.
* HUNTER: Simple level about handling beasts.
* DONTSLIDEAGAIN: A sequel to DONTSLIDE. This time, be clever.
* SOKYEBAN: Just a proof of concept. It shows how to include sokoban puzzles in a kye level.
* OBSCURE: It is all about a very obscure Kye 'feature' that may actually be an unintentional glitch but can be very useful when providing some puzzles.
* ALL : An intermediate level puzzle of the typical Kye 1.0 style, there are a couple of twists though.
* BLOCKADE : It is not obvious, but once you think of what to do here it can be very easy.
* BLOCK : This level is actually one of many levels of the same style I used to make when I was young. The levels I used to make followed the same lame ideas which are highlighted here. Showcases: Blocks as keys, Pushers as keys (And also the don't let pusher die challenge) and a sample of the magnet-monster challenge.
* TREASURE: Tons of diamonds are your reward for playing all of these levels. Though you better get ready for some artificial difficulty...


-----
I love Kye so much that I made a clone-extension called Xye, at http://xye.sourceforge.net . Perhaps it extends it too much because sometimes I feel like Kye is a purer puzzle. Though I find it fun to make levels for it.



